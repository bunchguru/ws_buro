﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web
Imports System.Web.Script.Services
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class WebService
    Inherits System.Web.Services.WebService
    Public PrimerNombre As String, SegundoNombre As String, ApellidoPaterno As String, ApellidoMaterno As String,
            RFC As String, Email As String, Calle As String, NoExt As String, NoInt As String, CPostal As String, Colonia As String,
        DelegMunic As String, Estado As String, TarjetaActiva As String, Digitos As String, Hipotecario As String, Automotriz As String,
        IDCont As Integer, IDPros As Integer, NoSol As String, valObs As String, valorRegresoCln As String
    <WebMethod>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    <ServiceModel.Web.WebGet(ResponseFormat:=ServiceModel.Web.WebMessageFormat.Json)>
    Public Function ConsultaBCScore(ByVal usuario As String, ByVal password As String, ByVal data As String) As String
        Dim cadenaDatos = decodBase64(data)
        Dim parametros = cadenaDatos.Split("&")
        Dim cliente = "ComparaGuru"
        Dim contrasena = "C0mparaGuru2018"
        Dim lengDatos = parametros.Length
        'Dim PrimerNombre, SegundoNombre, ApellidoPaterno, ApellidoMaterno, Calle, NoExt, NoInt, CPostal, Colonia, DelegMunic, Estado, TarjetaActiva, Digitos, Hipotecario, Automotriz As String
        For i = 0 To lengDatos - 1
            If parametros(i).Contains("primerNombre") Then
                PrimerNombre = parametros(i).Replace("primerNombre=", "")
            ElseIf parametros(i).Contains("segundoNombre") Then
                SegundoNombre = parametros(i).Replace("segundoNombre=", "")
            ElseIf parametros(i).Contains("apPat") Then
                ApellidoPaterno = parametros(i).Replace("apPat=", "")
            ElseIf parametros(i).Contains("apMat") Then
                ApellidoMaterno = parametros(i).Replace("apMat=", "")
            ElseIf parametros(i).Contains("rfc") Then
                RFC = parametros(i).Replace("rfc=", "")
            ElseIf parametros(i).Contains("calle") Then
                Calle = parametros(i).Replace("calle=", "")
            ElseIf parametros(i).Contains("noext") Then
                NoExt = parametros(i).Replace("noext=", "")
            ElseIf parametros(i).Contains("noint") Then
                NoInt = parametros(i).Replace("noint=", "")
            ElseIf parametros(i).Contains("cpostal") Then
                CPostal = parametros(i).Replace("cpostal=", "")
            ElseIf parametros(i).Contains("colonia") Then
                Colonia = parametros(i).Replace("colonia=", "")
            ElseIf parametros(i).Contains("deleg") Then
                DelegMunic = parametros(i).Replace("deleg=", "")
            ElseIf parametros(i).Contains("estado") Then
                Estado = parametros(i).Replace("estado=", "")
            ElseIf parametros(i).Contains("tarjeta") Then
                TarjetaActiva = parametros(i).Replace("tarjeta=", "")
            ElseIf parametros(i).Contains("digitos") Then
                Digitos = parametros(i).Replace("digitos=", "")
            ElseIf parametros(i).Contains("hipotecario") Then
                Hipotecario = parametros(i).Replace("hipotecario=", "")
            ElseIf parametros(i).Contains("automotriz") Then
                Automotriz = parametros(i).Replace("automotriz=", "")
            ElseIf parametros(i).Contains("email") Then
                Email = parametros(i).Replace("email=", "")
            End If
        Next
        Dim respuesta As New BCServiceProd.RespuestaBC
        Dim valorRegreso As String = Nothing
        If usuario = cliente And password = contrasena Then
            Try
                Dim objBC As New BCServiceProd.consultaXMLRequest
                objBC.Consulta = New BCServiceProd.ConsultaBC
                objBC.Consulta.Personas = New BCServiceProd.PersonasBC
                objBC.Consulta.Personas.Persona = New BCServiceProd.PersonaBC
                objBC.Consulta.Personas.Persona.Encabezado = New BCServiceProd.EncabezadoBC
                objBC.Consulta.Personas.Persona.Encabezado.Version = "12"
                objBC.Consulta.Personas.Persona.Encabezado.NumeroReferenciaOperador = "122000854270 1 7086142637"
                'objBC.Consulta.Personas.Persona.Encabezado.ProductoRequerido = "007"
                objBC.Consulta.Personas.Persona.Encabezado.ProductoRequerido = "107"
                objBC.Consulta.Personas.Persona.Encabezado.ClavePais = "MX"
                objBC.Consulta.Personas.Persona.Encabezado.IdentificadorBuro = "0000"
                'objBC.Consulta.Personas.Persona.Encabezado.ClaveUsuario = "UU78781001" 'prod1
                'objBC.Consulta.Personas.Persona.Encabezado.Password = "pf2h5n08" 'prod1
                objBC.Consulta.Personas.Persona.Encabezado.ClaveUsuario = "UU78781002" 'prod2
                objBC.Consulta.Personas.Persona.Encabezado.Password = "MSog2sJj" '"5O1ash9V" '"pf2H5n0L"'MSog2sJj' "OGPami9h" 'prod2s    Pass: qKh20Nrg
                'objBC.Consulta.Personas.Persona.Encabezado.ClaveUsuario = "ZM78781001" 'pruebas
                'objBC.Consulta.Personas.Persona.Encabezado.Password = "2W4feSea" 'pruebas
                objBC.Consulta.Personas.Persona.Encabezado.TipoConsulta = "I"
                objBC.Consulta.Personas.Persona.Encabezado.TipoContrato = "CC"
                objBC.Consulta.Personas.Persona.Encabezado.ClaveUnidadMonetaria = "MX"
                objBC.Consulta.Personas.Persona.Encabezado.Idioma = "ES"
                objBC.Consulta.Personas.Persona.Encabezado.TipoSalida = "01"

                objBC.Consulta.Personas.Persona.Nombre = New BCServiceProd.NombreBC
                objBC.Consulta.Personas.Persona.Nombre.ApellidoPaterno = ApellidoPaterno
                objBC.Consulta.Personas.Persona.Nombre.ApellidoMaterno = ApellidoMaterno
                objBC.Consulta.Personas.Persona.Nombre.PrimerNombre = PrimerNombre
                objBC.Consulta.Personas.Persona.Nombre.SegundoNombre = SegundoNombre
                objBC.Consulta.Personas.Persona.Nombre.RFC = RFC

                objBC.Consulta.Personas.Persona.Domicilios = New BCServiceProd.Direccion(0) {}
                objBC.Consulta.Personas.Persona.Domicilios(0) = New BCServiceProd.Direccion
                objBC.Consulta.Personas.Persona.Domicilios(0).Direccion1 = Calle & " " & NoExt & " " & NoInt
                objBC.Consulta.Personas.Persona.Domicilios(0).ColoniaPoblacion = Colonia
                objBC.Consulta.Personas.Persona.Domicilios(0).DelegacionMunicipio = DelegMunic
                objBC.Consulta.Personas.Persona.Domicilios(0).Ciudad = DelegMunic
                objBC.Consulta.Personas.Persona.Domicilios(0).Estado = IIf(Estado = "Distrito Federal", "CDMX", Estado)
                objBC.Consulta.Personas.Persona.Domicilios(0).CP = CPostal

                objBC.Consulta.Personas.Persona.Empleos = New BCServiceProd.EmpleoBC(0) {}
                objBC.Consulta.Personas.Persona.Empleos(0) = New BCServiceProd.EmpleoBC

                objBC.Consulta.Personas.Persona.Autentica = New BCServiceProd.AutenticacionBC
                objBC.Consulta.Personas.Persona.Autentica.TipoReporte = "RCN"
                objBC.Consulta.Personas.Persona.Autentica.TipoSalidaAU = "1"
                objBC.Consulta.Personas.Persona.Autentica.ReferenciaOperador = "COMPARAGURUCOM           "
                objBC.Consulta.Personas.Persona.Autentica.TarjetaCredito = TarjetaActiva
                objBC.Consulta.Personas.Persona.Autentica.UltimosCuatroDigitos = Digitos
                objBC.Consulta.Personas.Persona.Autentica.EjercidoCreditoHipotecario = Hipotecario
                objBC.Consulta.Personas.Persona.Autentica.EjercidoCreditoAutomotriz = Automotriz
                Dim hora As String = Date.Now.ToString("dd-MM-yy-hh-mm")
                Dim objBCConsumo As New BCServiceProd.WSConsultaDelegateClient
                Dim objSerialization As XmlSerializer = New XmlSerializer(GetType(BCServiceProd.ConsultaBC))
                Using writer As TextWriter = New StreamWriter("W:\XmlBuro\XmlEnvio-MASIVO-" & hora & "-" & RFC & ".xml")
                    objSerialization.Serialize(writer, objBC.Consulta)
                End Using

                respuesta = objBCConsumo.consultaXML(objBC.Consulta)
                Dim objSerialization2 As XmlSerializer = New XmlSerializer(GetType(BCServiceProd.RespuestaBC))
                Using writer As TextWriter = New StreamWriter("W:\XmlBuro\XmlRespuesta-MASIVO-" & hora & "-" & RFC & ".xml")
                    objSerialization2.Serialize(writer, respuesta)
                End Using
                'respuesta.Personas
            Catch ex As Exception
                xmlWS(ex.ToString)
            End Try
            Try
                'objSerialization2.Deserialize()
                Dim Resp As BCServiceProd.PersonaRespBC = respuesta.Personas(0)
                Dim TipoErrorAR As String = Nothing
                Dim TipoErrorUR As String = Nothing
                Dim CodigoError As String = Nothing
                Dim CodigoScore As String = Nothing
                Dim ValorScore As String = Nothing
                Dim CodRazon As String() = Nothing
                Dim CodRazonTxt As String = Nothing
                Dim valScoreInt As Integer = Nothing
                If Resp.Error IsNot Nothing Then
                    If Resp.Error.AR IsNot Nothing Then
                        If Resp.Error.AR.ClaveOPasswordErroneo IsNot Nothing Then
                            TipoErrorAR = Resp.Error.AR.ClaveOPasswordErroneo
                            valorRegreso = "<b>Error: No presenta registro de respuesta. <br> Motivo: " & TipoErrorAR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "<b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "<br>", "")
                        ElseIf Resp.Error.AR.ErrorSistemaBC IsNot Nothing Then
                            TipoErrorAR = Resp.Error.AR.ErrorSistemaBC
                            valorRegreso = "<b>Error: No presenta registro de respuesta. <br> Motivo: " & TipoErrorAR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "<b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "<br>", "")
                        ElseIf Resp.Error.AR.EtiquetaSegmentoErronea IsNot Nothing Then
                            TipoErrorAR = Resp.Error.AR.EtiquetaSegmentoErronea
                            valorRegreso = "<b>Error: No presenta registro de respuesta. <br> Motivo: " & TipoErrorAR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "<b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "<br>", "")
                        ElseIf Resp.Error.AR.FaltaCampoRequerido IsNot Nothing Then
                            TipoErrorAR = Resp.Error.AR.FaltaCampoRequerido
                            valorRegreso = "<b>Error: No presenta registro de respuesta. <br> Motivo: " & TipoErrorAR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "<b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "<br>", "")
                        ElseIf Resp.Error.AR.SujetoNoAutenticado IsNot Nothing Then
                            TipoErrorAR = Resp.Error.AR.SujetoNoAutenticado
                            Dim noAut(1) As String
                            Dim CodNoAut, SubCodNoAut, Motivo, SubMotivo As String
                            noAut = TipoErrorAR.Split("|")
                            CodNoAut = noAut(1)
                            CodNoAut = CodNoAut.Substring(0, 3)
                            SubCodNoAut = noAut(1).Substring(3, 2)
                            Select Case CodNoAut
                                Case "405"
                                    Motivo = "Tarjeta de Crédito Existente"
                                    Select Case SubCodNoAut
                                        Case "01"
                                            SubMotivo = "Tarjeta de Crédito Bancaria Existente"
                                        Case "02"
                                            SubMotivo = "Tarjeta de Crédito Bancaria y Departamental Existente"
                                    End Select
                                Case "406"
                                    Motivo = "Tarjeta de Crédito No Existente"
                                Case "407"
                                    Motivo = "Tarjeta de Crédito No Coincide con lo reportado en la Base de Datos."
                                    Select Case SubCodNoAut
                                        Case "01"
                                            SubMotivo = "Tarjeta de Crédito No Coincide con lo reportado en la Base de Datos"
                                        Case "02"
                                            SubMotivo = "Tarjeta de Crédito con coincidencias parciales"
                                        Case "03"
                                            SubMotivo = "Tarjeta Departamental o con Fecha de Cierre"
                                        Case "04"
                                            SubMotivo = "Tarjeta de Crédito coincide con los 4 primeros dígitos"
                                        Case "05"
                                            SubMotivo = "Tarjeta de Crédito 1234 ó 0000"
                                    End Select
                                Case "409"
                                    Motivo = "Crédito Hipotecario Existente"
                                    SubMotivo = ""
                                Case "410"
                                    SubMotivo = ""
                                    Motivo = "Crédito Hipotecario No Existente"
                                Case "412"
                                    SubMotivo = ""
                                    Motivo = "Crédito Automotriz Existente"
                                Case "413"
                                    SubMotivo = ""
                                    Motivo = "Crédito Automotriz No Existente"
                            End Select

                            valorRegreso = "<b>Error: No presenta registro de respuesta. <br> " & noAut(0) & "Motivo: " & Motivo & " - " & SubMotivo & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "<b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "<br>", "")
                        End If
                        generateLayoutBC(Resp, valorRegresoCln)
                    End If

                    If Resp.Error.UR IsNot Nothing Then
                        If Resp.Error.UR.ErrorReporteBloqueado IsNot Nothing Then
                            TipoErrorUR = "Reporte Bloqueado - " & Resp.Error.UR.ErrorReporteBloqueado
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.ErrorSistemaBuroCredito IsNot Nothing Then
                            TipoErrorUR = "Error Sistema Buró Crédito - " & Resp.Error.UR.ErrorSistemaBuroCredito
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.EtiquetaSegmentoErronea IsNot Nothing Then
                            TipoErrorUR = "Etiqueta Segmento Erronea - " & Resp.Error.UR.EtiquetaSegmentoErronea
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.FaltaCampoRequerido IsNot Nothing Then
                            TipoErrorUR = "Falta Campo Requerido - " & Resp.Error.UR.FaltaCampoRequerido
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.InformacionErroneaParaConsulta IsNot Nothing Then
                            TipoErrorUR = "Información errónea para consultar - " & Resp.Error.UR.InformacionErroneaParaConsulta
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.NumeroErroneoSegmentos IsNot Nothing Then
                            TipoErrorUR = "Numero Erroneo Segmentos - " & Resp.Error.UR.NumeroErroneoSegmentos
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.OrdenErroneoSegmento IsNot Nothing Then
                            TipoErrorUR = "Orden Erroneo Segmento - " & Resp.Error.UR.OrdenErroneoSegmento
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.PasswordOClaveErronea IsNot Nothing Then
                            TipoErrorUR = "Password O Clave Erronea - " & Resp.Error.UR.PasswordOClaveErronea
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.ProductoSolicitadoErroneo IsNot Nothing Then
                            TipoErrorUR = "Producto Solicitado Erroneo - " & Resp.Error.UR.ProductoSolicitadoErroneo
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.SegmentoRequeridoNoProporcionado IsNot Nothing Then
                            TipoErrorUR = "Segmento Requerido No Proporcionado - " & Resp.Error.UR.SegmentoRequeridoNoProporcionado
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.SolicitudClienteErronea IsNot Nothing Then
                            TipoErrorUR = "Solicitud Cliente Erronea - " & Resp.Error.UR.SolicitudClienteErronea
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.UltimaInformacionValidaCliente IsNot Nothing Then
                            TipoErrorUR = "Ultima Informacion Valida Cliente - " & Resp.Error.UR.UltimaInformacionValidaCliente
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                        ElseIf Resp.Error.UR.ValorErroneoCampoRelacionado IsNot Nothing Then
                            TipoErrorUR = "Valor Erroneo Campo Relacionado - " & Resp.Error.UR.ValorErroneoCampoRelacionado
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.VersionProporcionadaErronea IsNot Nothing Then
                            TipoErrorUR = "Version Proporcionada Erronea - " & Resp.Error.UR.VersionProporcionadaErronea
                            valorRegreso = "</b>Error: " & TipoErrorUR & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        End If
                        'generateLayoutBC(Resp, valorRegresoCln)
                    End If
                Else
                    If Resp.ScoreBuroCredito(0).CodigoError IsNot Nothing Then
                        CodigoError = Resp.ScoreBuroCredito(0).CodigoError
                        Select Case CodigoError
                            Case "03"
                                valorRegreso = "</b>Error: Score No Disponible</b>"
                                valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            Case "02"
                                valorRegreso = "</b>Error: Solicitud de Score Invalida</b>"
                                valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            Case "01"
                                valorRegreso = "</b>Error: Solicitud No Autorizada</b>"
                                valorRegresoCln = Replace(valorRegreso, "</b>", "")
                        End Select
                        generateLayoutBC(Resp, valorRegresoCln)
                    Else
                        CodigoScore = Resp.ScoreBuroCredito(0).CodigoScore
                        ValorScore = Resp.ScoreBuroCredito(0).ValorScore
                        Select Case ValorScore
                            Case "-001"
                                valorRegreso = "<b>Exclusión: Consumidor Fallecido</b>"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case "-005"
                                valorRegreso = "<b>Exclusión: Expediente con todas las cuentas cerradas y por lo menos con una en atraso mayor o igual a 90 días</b>"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case "-006"
                                valorRegreso = "<b>Exclusión: Expediente con todas sus cuentas con antigüedad menor a 6 meses y al menos una tiene MOP >= 03</b>"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case "-007"
                                valorRegreso = "<b>Exclusión: Expediente con todas sus cuentas con antigüedad menor a 6 meses y al menos una tiene MOP >= 02</b>"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case "-008"
                                valorRegreso = "<b>Exclusión: Expediente no tiene al menos una cuenta actualizada en el último año o con antigüedad mínima de 6 meses, y/o con cuentas que no se incluyen en el cálculo del BC-Score</b>"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case "-009"
                                valorRegreso = "<b>Exclusión: Expediente sin cuentas para cálculo de BC-Score</b>"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case Else
                                CodRazon = Resp.ScoreBuroCredito(0).CodigoRazon
                                If CodRazon IsNot Nothing Then
                                    For i As Integer = 0 To CodRazon.Length - 1
                                        CodRazonTxt += CodRazon(i) & " - " & GetValorCodRazon(CodRazon(i)) & "</br>"
                                    Next
                                End If
                                ValorScore = Resp.ScoreBuroCredito(0).ValorScore.ToString
                                valScoreInt = Convert.ToInt32(ValorScore)
                                valorRegreso = "BC Score: " + valScoreInt.ToString + " </br> Motivos: </br> " + CodRazonTxt + "</b></span>"
                                valorRegresoCln = "BC Score: " + valScoreInt.ToString + " Motivos: " + CodRazonTxt
                                generateLayoutBC(Resp, valScoreInt)
                        End Select
                    End If
                End If

                IDCont = checkExistsContact()
                If IDCont <> 0 Then
                    'generateProspect(IDCont, valorRegresoCln)
                Else
                    'If generateContact() = True Then
                    'generateProspect(IDCont, valorRegresoCln)
                    'End If
                End If

            Catch ex As Exception
                xmlWS(ex.ToString)
                Dim Errror = "El servicio no esta disponible, por favor intentelo más tarde"
                Return ex.ToString
            End Try
        Else
            Dim errorAutentificacion = "Error de autentificación"
            Return errorAutentificacion
        End If
        'Context.Response.Write(valorRegreso)
        valorRegreso = Replace(valorRegreso, "<b>", "")
        valorRegreso = Replace(valorRegreso, "</b>", "")
        'Context.Response.Write(valorRegreso)
        Return valorRegreso

    End Function

    <WebMethod>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)>
    <ServiceModel.Web.WebGet(ResponseFormat:=ServiceModel.Web.WebMessageFormat.Json)>
    Public Sub ConsultaBCScoreJSON(ByVal usuario As String, ByVal password As String, ByVal data As String)
        Dim cadenaDatos = decodBase64(data)
        Dim parametros = cadenaDatos.Split("&")
        Dim cliente = "ComparaGuru"
        Dim contrasena = "C0mparaGuru2018"
        Dim lengDatos = parametros.Length
        'Dim PrimerNombre, SegundoNombre, ApellidoPaterno, ApellidoMaterno, Calle, NoExt, NoInt, CPostal, Colonia, DelegMunic, Estado, TarjetaActiva, Digitos, Hipotecario, Automotriz As String
        For i = 0 To lengDatos - 1
            If parametros(i).Contains("primerNombre") Then
                PrimerNombre = parametros(i).Replace("primerNombre=", "")
            ElseIf parametros(i).Contains("segundoNombre") Then
                SegundoNombre = parametros(i).Replace("segundoNombre=", "")
            ElseIf parametros(i).Contains("apPat") Then
                ApellidoPaterno = parametros(i).Replace("apPat=", "")
            ElseIf parametros(i).Contains("apMat") Then
                ApellidoMaterno = parametros(i).Replace("apMat=", "")
            ElseIf parametros(i).Contains("rfc") Then
                RFC = parametros(i).Replace("rfc=", "")
            ElseIf parametros(i).Contains("calle") Then
                Calle = parametros(i).Replace("calle=", "")
            ElseIf parametros(i).Contains("noext") Then
                NoExt = parametros(i).Replace("noext=", "")
            ElseIf parametros(i).Contains("noint") Then
                NoInt = parametros(i).Replace("noint=", "")
            ElseIf parametros(i).Contains("cpostal") Then
                CPostal = parametros(i).Replace("cpostal=", "")
            ElseIf parametros(i).Contains("colonia") Then
                Colonia = parametros(i).Replace("colonia=", "")
            ElseIf parametros(i).Contains("deleg") Then
                DelegMunic = parametros(i).Replace("deleg=", "")
            ElseIf parametros(i).Contains("estado") Then
                Estado = parametros(i).Replace("estado=", "")
            ElseIf parametros(i).Contains("tarjeta") Then
                TarjetaActiva = parametros(i).Replace("tarjeta=", "")
            ElseIf parametros(i).Contains("digitos") Then
                Digitos = parametros(i).Replace("digitos=", "")
            ElseIf parametros(i).Contains("hipotecario") Then
                Hipotecario = parametros(i).Replace("hipotecario=", "")
            ElseIf parametros(i).Contains("automotriz") Then
                Automotriz = parametros(i).Replace("automotriz=", "")
            ElseIf parametros(i).Contains("email") Then
                Email = parametros(i).Replace("email=", "")
            End If
        Next
        Dim respuesta As New BCServiceProd.RespuestaBC
        Dim valorRegreso As String = Nothing
        If usuario = cliente And password = contrasena Then
            Try
                Dim objBC As New BCServiceProd.consultaXMLRequest
                objBC.Consulta = New BCServiceProd.ConsultaBC
                objBC.Consulta.Personas = New BCServiceProd.PersonasBC
                objBC.Consulta.Personas.Persona = New BCServiceProd.PersonaBC
                objBC.Consulta.Personas.Persona.Encabezado = New BCServiceProd.EncabezadoBC
                objBC.Consulta.Personas.Persona.Encabezado.Version = "12"
                objBC.Consulta.Personas.Persona.Encabezado.NumeroReferenciaOperador = "122000854270 1 7086142637"
                'objBC.Consulta.Personas.Persona.Encabezado.ProductoRequerido = "007"
                objBC.Consulta.Personas.Persona.Encabezado.ProductoRequerido = "107"
                objBC.Consulta.Personas.Persona.Encabezado.ClavePais = "MX"
                objBC.Consulta.Personas.Persona.Encabezado.IdentificadorBuro = "0000"
                'objBC.Consulta.Personas.Persona.Encabezado.ClaveUsuario = "UU78781001" 'prod1
                'objBC.Consulta.Personas.Persona.Encabezado.Password = "pf2h5n08" 'prod1
                objBC.Consulta.Personas.Persona.Encabezado.ClaveUsuario = "UU78781002" 'prod2
                objBC.Consulta.Personas.Persona.Encabezado.Password = "MSog2sJj" '"5O1ash9V" '"pf2H5n0L"'MSog2sJj' "OGPami9h" 'prod2s    Pass: qKh20Nrg
                'objBC.Consulta.Personas.Persona.Encabezado.ClaveUsuario = "ZM78781001" 'pruebas
                'objBC.Consulta.Personas.Persona.Encabezado.Password = "2W4feSea" 'pruebas
                objBC.Consulta.Personas.Persona.Encabezado.TipoConsulta = "I"
                objBC.Consulta.Personas.Persona.Encabezado.TipoContrato = "CC"
                objBC.Consulta.Personas.Persona.Encabezado.ClaveUnidadMonetaria = "MX"
                objBC.Consulta.Personas.Persona.Encabezado.Idioma = "ES"
                objBC.Consulta.Personas.Persona.Encabezado.TipoSalida = "01"

                objBC.Consulta.Personas.Persona.Nombre = New BCServiceProd.NombreBC
                objBC.Consulta.Personas.Persona.Nombre.ApellidoPaterno = ApellidoPaterno
                objBC.Consulta.Personas.Persona.Nombre.ApellidoMaterno = ApellidoMaterno
                objBC.Consulta.Personas.Persona.Nombre.PrimerNombre = PrimerNombre
                objBC.Consulta.Personas.Persona.Nombre.SegundoNombre = SegundoNombre
                objBC.Consulta.Personas.Persona.Nombre.RFC = RFC

                objBC.Consulta.Personas.Persona.Domicilios = New BCServiceProd.Direccion(0) {}
                objBC.Consulta.Personas.Persona.Domicilios(0) = New BCServiceProd.Direccion
                objBC.Consulta.Personas.Persona.Domicilios(0).Direccion1 = Calle & " " & NoExt & " " & NoInt
                objBC.Consulta.Personas.Persona.Domicilios(0).ColoniaPoblacion = Colonia
                objBC.Consulta.Personas.Persona.Domicilios(0).DelegacionMunicipio = DelegMunic
                objBC.Consulta.Personas.Persona.Domicilios(0).Ciudad = DelegMunic
                objBC.Consulta.Personas.Persona.Domicilios(0).Estado = GetEstado(Estado) 'IIf(Estado = "Distrito Federal", "CDMX", Estado)
                objBC.Consulta.Personas.Persona.Domicilios(0).CP = CPostal

                objBC.Consulta.Personas.Persona.Empleos = New BCServiceProd.EmpleoBC(0) {}
                objBC.Consulta.Personas.Persona.Empleos(0) = New BCServiceProd.EmpleoBC

                objBC.Consulta.Personas.Persona.Autentica = New BCServiceProd.AutenticacionBC
                objBC.Consulta.Personas.Persona.Autentica.TipoReporte = "RCN"
                objBC.Consulta.Personas.Persona.Autentica.TipoSalidaAU = "1"
                objBC.Consulta.Personas.Persona.Autentica.ReferenciaOperador = "COMPARAGURUCOM           "
                objBC.Consulta.Personas.Persona.Autentica.TarjetaCredito = Replace(Replace(TarjetaActiva, "Si", "V"), "No", "F")
                objBC.Consulta.Personas.Persona.Autentica.UltimosCuatroDigitos = Digitos
                objBC.Consulta.Personas.Persona.Autentica.EjercidoCreditoHipotecario = Replace(Replace(Hipotecario, "Si", "V"), "No", "F")
                objBC.Consulta.Personas.Persona.Autentica.EjercidoCreditoAutomotriz = Replace(Replace(Automotriz, "Si", "V"), "No", "F")
                Dim hora As String = Date.Now.ToString("dd-MM-yy-hh-mm")
                Dim objBCConsumo As New BCServiceProd.WSConsultaDelegateClient
                Dim objSerialization As XmlSerializer = New XmlSerializer(GetType(BCServiceProd.ConsultaBC))
                Using writer As TextWriter = New StreamWriter("W:\XmlBuro\XmlEnvio-" & hora & ".xml")
                    objSerialization.Serialize(writer, objBC.Consulta)
                End Using

                respuesta = objBCConsumo.consultaXML(objBC.Consulta)
                Dim objSerialization2 As XmlSerializer = New XmlSerializer(GetType(BCServiceProd.RespuestaBC))
                Using writer As TextWriter = New StreamWriter("W:\XmlBuro\XmlRespuesta-" & hora & ".xml")
                    objSerialization2.Serialize(writer, respuesta)
                End Using
                'respuesta.Personas
            Catch ex As Exception
                xmlWS(ex.ToString)
            End Try
            Try
                'objSerialization2.Deserialize()
                Dim Resp As BCServiceProd.PersonaRespBC = respuesta.Personas(0)
                Dim TipoErrorAR As String = Nothing
                Dim TipoErrorUR As String = Nothing
                Dim CodigoError As String = Nothing
                Dim CodigoScore As String = Nothing
                Dim ValorScore As String = Nothing
                Dim CodRazon As String() = Nothing
                Dim CodRazonTxt As String = Nothing
                Dim valScoreInt As Integer = Nothing
                If Resp.Error IsNot Nothing Then
                    If Resp.Error.AR IsNot Nothing Then
                        If Resp.Error.AR.ClaveOPasswordErroneo IsNot Nothing Then
                            TipoErrorAR = Resp.Error.AR.ClaveOPasswordErroneo
                            valorRegreso = "Error: No presenta registro de respuesta. Motivo: " & TipoErrorAR
                            valorRegresoCln = Replace(valorRegreso, "<b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "<br>", "")
                        ElseIf Resp.Error.AR.ErrorSistemaBC IsNot Nothing Then
                            TipoErrorAR = Resp.Error.AR.ErrorSistemaBC
                            valorRegreso = "Error: No presenta registro de respuesta. Motivo: " & TipoErrorAR
                            valorRegresoCln = Replace(valorRegreso, "<b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "<br>", "")
                        ElseIf Resp.Error.AR.EtiquetaSegmentoErronea IsNot Nothing Then
                            TipoErrorAR = Resp.Error.AR.EtiquetaSegmentoErronea
                            valorRegreso = "Error: No presenta registro de respuesta. Motivo: " & TipoErrorAR
                            valorRegresoCln = Replace(valorRegreso, "<b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "<br>", "")
                        ElseIf Resp.Error.AR.FaltaCampoRequerido IsNot Nothing Then
                            TipoErrorAR = Resp.Error.AR.FaltaCampoRequerido
                            valorRegreso = "Error: No presenta registro de respuesta. Motivo: " & TipoErrorAR
                            valorRegresoCln = Replace(valorRegreso, "<b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "<br>", "")
                        ElseIf Resp.Error.AR.SujetoNoAutenticado IsNot Nothing Then
                            TipoErrorAR = Resp.Error.AR.SujetoNoAutenticado
                            Dim noAut(1) As String
                            Dim CodNoAut, SubCodNoAut, Motivo, SubMotivo As String
                            noAut = TipoErrorAR.Split("|")
                            CodNoAut = noAut(1)
                            CodNoAut = CodNoAut.Substring(0, 3)
                            SubCodNoAut = noAut(1).Substring(3, 2)
                            Select Case CodNoAut
                                Case "405"
                                    Motivo = "Tarjeta de Crédito Existente"
                                    Select Case SubCodNoAut
                                        Case "01"
                                            SubMotivo = "Tarjeta de Crédito Bancaria Existente"
                                        Case "02"
                                            SubMotivo = "Tarjeta de Crédito Bancaria y Departamental Existente"
                                    End Select
                                Case "406"
                                    Motivo = "Tarjeta de Crédito No Existente"
                                Case "407"
                                    Motivo = "Tarjeta de Crédito No Coincide con lo reportado en la Base de Datos."
                                    Select Case SubCodNoAut
                                        Case "01"
                                            SubMotivo = "Tarjeta de Crédito No Coincide con lo reportado en la Base de Datos"
                                        Case "02"
                                            SubMotivo = "Tarjeta de Crédito con coincidencias parciales"
                                        Case "03"
                                            SubMotivo = "Tarjeta Departamental o con Fecha de Cierre"
                                        Case "04"
                                            SubMotivo = "Tarjeta de Crédito coincide con los 4 primeros dígitos"
                                        Case "05"
                                            SubMotivo = "Tarjeta de Crédito 1234 ó 0000"
                                    End Select
                                Case "409"
                                    Motivo = "Crédito Hipotecario Existente"
                                    SubMotivo = ""
                                Case "410"
                                    SubMotivo = ""
                                    Motivo = "Crédito Hipotecario No Existente"
                                Case "412"
                                    SubMotivo = ""
                                    Motivo = "Crédito Automotriz Existente"
                                Case "413"
                                    SubMotivo = ""
                                    Motivo = "Crédito Automotriz No Existente"
                            End Select

                            valorRegreso = "<b>Error: No presenta registro de respuesta. <br> " & noAut(0) & "Motivo: " & Motivo & " - " & SubMotivo & "</b>"
                            valorRegresoCln = Replace(valorRegreso, "<b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                            valorRegresoCln = Replace(valorRegresoCln, "<br>", "")
                        End If
                        generateLayoutBC(Resp, valorRegresoCln)
                    End If

                    If Resp.Error.UR IsNot Nothing Then
                        If Resp.Error.UR.ErrorReporteBloqueado IsNot Nothing Then
                            TipoErrorUR = "Reporte Bloqueado - " & Resp.Error.UR.ErrorReporteBloqueado
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.ErrorSistemaBuroCredito IsNot Nothing Then
                            TipoErrorUR = "Error Sistema Buró Crédito - " & Resp.Error.UR.ErrorSistemaBuroCredito
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.EtiquetaSegmentoErronea IsNot Nothing Then
                            TipoErrorUR = "Etiqueta Segmento Erronea - " & Resp.Error.UR.EtiquetaSegmentoErronea
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.FaltaCampoRequerido IsNot Nothing Then
                            TipoErrorUR = "Falta Campo Requerido - " & Resp.Error.UR.FaltaCampoRequerido
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.InformacionErroneaParaConsulta IsNot Nothing Then
                            TipoErrorUR = "Información errónea para consultar - " & Resp.Error.UR.InformacionErroneaParaConsulta
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.NumeroErroneoSegmentos IsNot Nothing Then
                            TipoErrorUR = "Numero Erroneo Segmentos - " & Resp.Error.UR.NumeroErroneoSegmentos
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.OrdenErroneoSegmento IsNot Nothing Then
                            TipoErrorUR = "Orden Erroneo Segmento - " & Resp.Error.UR.OrdenErroneoSegmento
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.PasswordOClaveErronea IsNot Nothing Then
                            TipoErrorUR = "Password O Clave Erronea - " & Resp.Error.UR.PasswordOClaveErronea
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.ProductoSolicitadoErroneo IsNot Nothing Then
                            TipoErrorUR = "Producto Solicitado Erroneo - " & Resp.Error.UR.ProductoSolicitadoErroneo
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.SegmentoRequeridoNoProporcionado IsNot Nothing Then
                            TipoErrorUR = "Segmento Requerido No Proporcionado - " & Resp.Error.UR.SegmentoRequeridoNoProporcionado
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.SolicitudClienteErronea IsNot Nothing Then
                            TipoErrorUR = "Solicitud Cliente Erronea - " & Resp.Error.UR.SolicitudClienteErronea
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.UltimaInformacionValidaCliente IsNot Nothing Then
                            TipoErrorUR = "Ultima Informacion Valida Cliente - " & Resp.Error.UR.UltimaInformacionValidaCliente
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                        ElseIf Resp.Error.UR.ValorErroneoCampoRelacionado IsNot Nothing Then
                            TipoErrorUR = "Valor Erroneo Campo Relacionado - " & Resp.Error.UR.ValorErroneoCampoRelacionado
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        ElseIf Resp.Error.UR.VersionProporcionadaErronea IsNot Nothing Then
                            TipoErrorUR = "Version Proporcionada Erronea - " & Resp.Error.UR.VersionProporcionadaErronea
                            valorRegreso = "Error: " & TipoErrorUR
                            valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            generateLayoutBC(Resp, valorRegresoCln)
                        End If
                        'generateLayoutBC(Resp, valorRegresoCln)
                    End If
                Else
                    If Resp.ScoreBuroCredito(0).CodigoError IsNot Nothing Then
                        CodigoError = Resp.ScoreBuroCredito(0).CodigoError
                        Select Case CodigoError
                            Case "03"
                                valorRegreso = "Error: Score No Disponible"
                                valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            Case "02"
                                valorRegreso = "Error: Solicitud de Score Invalida"
                                valorRegresoCln = Replace(valorRegreso, "</b>", "")
                            Case "01"
                                valorRegreso = "Error: Solicitud No Autorizada"
                                valorRegresoCln = Replace(valorRegreso, "</b>", "")
                        End Select
                        generateLayoutBC(Resp, valorRegresoCln)
                    Else
                        CodigoScore = Resp.ScoreBuroCredito(0).CodigoScore
                        ValorScore = Resp.ScoreBuroCredito(0).ValorScore
                        Select Case ValorScore
                            Case "-001"
                                valorRegreso = "Exclusión: Consumidor Fallecido"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case "-005"
                                valorRegreso = "Exclusión: Expediente con todas las cuentas cerradas y por lo menos con una en atraso mayor o igual a 90 días"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case "-006"
                                valorRegreso = "Exclusión: Expediente con todas sus cuentas con antigüedad menor a 6 meses y al menos una tiene MOP >= 03"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case "-007"
                                valorRegreso = "Exclusión: Expediente con todas sus cuentas con antigüedad menor a 6 meses y al menos una tiene MOP >= 02"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case "-008"
                                valorRegreso = "Exclusión: Expediente no tiene al menos una cuenta actualizada en el último año o con antigüedad mínima de 6 meses, y/o con cuentas que no se incluyen en el cálculo del BC-Score"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case "-009"
                                valorRegreso = "Exclusión: Expediente sin cuentas para cálculo de BC-Score"
                                valorRegresoCln = Replace(valorRegreso, "<b>", "")
                                valorRegresoCln = Replace(valorRegresoCln, "</b>", "")
                                generateLayoutBC(Resp, valorRegresoCln)
                            Case Else
                                CodRazon = Resp.ScoreBuroCredito(0).CodigoRazon
                                If CodRazon IsNot Nothing Then
                                    For i As Integer = 0 To CodRazon.Length - 1
                                        CodRazonTxt += CodRazon(i) & " - " & GetValorCodRazon(CodRazon(i)) & " "
                                    Next
                                End If
                                ValorScore = Resp.ScoreBuroCredito(0).ValorScore.ToString
                                valScoreInt = Convert.ToInt32(ValorScore)
                                valorRegreso = "BC Score: " + valScoreInt.ToString + " Motivos: " + CodRazonTxt
                                valorRegresoCln = "BC Score: " + valScoreInt.ToString + " Motivos: " + CodRazonTxt
                                generateLayoutBC(Resp, valScoreInt)
                        End Select
                    End If
                End If

                IDCont = checkExistsContact()
                If IDCont <> 0 Then
                    'generateProspect(IDCont, valorRegresoCln)
                Else
                    'If generateContact() = True Then
                    'generateProspect(IDCont, valorRegresoCln)
                    'End If
                End If

            Catch ex As Exception
                xmlWS(ex.ToString)
                Dim Errror = "El servicio no esta disponible, por favor intentelo más tarde " + ex.ToString
                'Return ex.ToString
                Context.Response.Write(Errror)
            End Try
        Else
            Dim errorAutentificacion = "Error de autentificación"
            Context.Response.Write(errorAutentificacion)
            'Return errorAutentificacion
        End If
        'Context.Response.Write(valorRegreso)
        valorRegreso = Replace(valorRegreso, "<b>", "")
        valorRegreso = Replace(valorRegreso, "</b>", "")
        Context.Response.Write(valorRegreso)
        'Return valorRegreso
    End Sub

    Function GetEstado(ByVal Estado As String) As String
        Select Case Estado
            Case "Aguascalientes"
                Estado = "AGS"
            Case "Baja California"
                Estado = "BCN"
            Case "Baja California Sur"
                Estado = "BCS"
            Case "Campeche"
                Estado = "CAM"
            Case "Chiapas"
                Estado = "CHS"
            Case "Chihuahua"
                Estado = "CHI"
            Case "Coahuila de Zaragoza"
                Estado = "COA"
            Case "Colima"
                Estado = "COL"
            Case "Distrito Federal"
                Estado = "CDMX"
            Case "Durango"
                Estado = "DGO"
            Case "Estado de Mexico"
                Estado = "EM"
            Case "Guanajuato"
                Estado = "GTO"
            Case "Guerrero"
                Estado = "GRO"
            Case "Hidalgo"
                Estado = "HGO"
            Case "Jalisco"
                Estado = "JAL"
            Case "Michoacan de Ocampo"
                Estado = "MICH"
            Case "Morelos"
                Estado = "MOR"
            Case "Nayarit"
                Estado = "NAY"
            Case "Nuevo Leon"
                Estado = "NL"
            Case "Oaxaca"
                Estado = "OAX"
            Case "Puebla"
                Estado = "PUE"
            Case "Queretaro"
                Estado = "QRO"
            Case "Quintana Roo"
                Estado = "QR"
            Case "San Luis Potosi"
                Estado = "SLP"
            Case "Sinaloa"
                Estado = "SIN"
            Case "Sonora"
                Estado = "SON"
            Case "Tabasco"
                Estado = "TAB"
            Case "Tamaulipas"
                Estado = "TAM"
            Case "Tlaxcala"
                Estado = "TLAX"
            Case "Veracruz"
                Estado = "VER"
            Case "Yucatan"
                Estado = "YUC"
            Case "Zacatecas"
                Estado = "ZAC"
        End Select
        Return Estado
    End Function
    Function decodBase64(ByVal cadena As String) As String
        Dim data() As Byte
        Dim enc8 As Encoding = Encoding.UTF8
        data = System.Convert.FromBase64String(cadena)
        'Dim base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data)
        Dim base64Decoded = System.Text.Encoding.UTF7.GetString(data)

        Return base64Decoded

    End Function

    Public Function generateLayoutBC(ByVal Respuesta As BCServiceProd.PersonaRespBC, ByVal Score As String) As Boolean
        Dim connectionString As String = "Data Source=23.251.146.119;Initial Catalog=BuroCredito;Persist Security Info=True;User Id=sa;Password=Inteldba01"
        Dim sql As String = "INSERT INTO LayoutBCScore ([No Solicitud], Fecha, Hora, [Nombre(s)], Paterno, Materno, RFC, [Calle - No], Colonia, " &
                            "Ciudad, [Edo.], [Tarjeta Crédito], [Ultimos 4 dígitos], Hipotecario, Automotriz, Autoriza, IdAutentica, [No. Control BC]) OUTPUT Inserted.No " &
                            "Values (@NumSolicitud, @Fecha, @Hora, @Nombres, @Paterno, @Materno, @RFC, @CalleNum, @Colonia, @Ciudad, @Estado, @Tarjeta, @Ultimos, @Hipotecario,  " &
                            "@Automotriz, @Autoriza, @IDAutentica, @NoControlBC) "
        'Dim hora As TimeSpan = TimeSpan.Parse(Date.Now.ToString("hh:mm:ss"))
        Dim NoControl As String = Nothing
        If Respuesta.Encabezado Is Nothing Then
            NoControl = ""
        Else
            NoControl = Respuesta.Encabezado.NumeroControlConsulta
        End If
        Using connection As New SqlConnection(connectionString)
            Dim command As New SqlCommand(sql, connection)
            command.Parameters.Add("@NumSolicitud", SqlDbType.NVarChar)
            command.Parameters("@NumSolicitud").Value = ""
            command.Parameters.Add("@Fecha", SqlDbType.NVarChar)
            command.Parameters("@Fecha").Value = Date.Now.ToString("dd-MM-yyyy")
            command.Parameters.Add("@Hora", SqlDbType.NVarChar)
            command.Parameters("@Hora").Value = Date.Now.ToString("hh:mm:ss")
            command.Parameters.Add("@Nombres", SqlDbType.NVarChar)
            command.Parameters("@Nombres").Value = PrimerNombre & " " & SegundoNombre
            command.Parameters.Add("@Paterno", SqlDbType.NVarChar)
            command.Parameters("@Paterno").Value = ApellidoPaterno
            command.Parameters.Add("@Materno", SqlDbType.NVarChar)
            command.Parameters("@Materno").Value = ApellidoMaterno
            command.Parameters.Add("@RFC", SqlDbType.NVarChar)
            command.Parameters("@RFC").Value = RFC
            command.Parameters.Add("@CalleNum", SqlDbType.NVarChar)
            command.Parameters("@CalleNum").Value = Calle & " " & NoExt & " " & NoInt
            command.Parameters.Add("@Colonia", SqlDbType.NVarChar)
            command.Parameters("@Colonia").Value = Colonia
            command.Parameters.Add("@Ciudad", SqlDbType.NVarChar)
            command.Parameters("@Ciudad").Value = DelegMunic
            command.Parameters.Add("@Estado", SqlDbType.NVarChar)
            command.Parameters("@Estado").Value = DelegMunic
            command.Parameters.Add("@Tarjeta", SqlDbType.NVarChar)
            command.Parameters("@Tarjeta").Value = TarjetaActiva
            command.Parameters.Add("@Ultimos", SqlDbType.NVarChar)
            command.Parameters("@Ultimos").Value = Digitos
            command.Parameters.Add("@Hipotecario", SqlDbType.NVarChar)
            command.Parameters("@Hipotecario").Value = Hipotecario
            command.Parameters.Add("@Automotriz", SqlDbType.NVarChar)
            command.Parameters("@Automotriz").Value = Automotriz
            command.Parameters.Add("@Autoriza", SqlDbType.NVarChar)
            command.Parameters("@Autoriza").Value = ""
            command.Parameters.Add("@IDAutentica", SqlDbType.NVarChar)
            command.Parameters("@IDAutentica").Value = ""
            command.Parameters.Add("@NoControlBC", SqlDbType.NVarChar)
            command.Parameters("@NoControlBC").Value = NoControl
            saveScore(Score, NoControl)
            Try
                connection.Open()
                Dim sdr As SqlDataReader = command.ExecuteReader()
                sdr.Read()
                NoSol = sdr("No")
                connection.Close()

                Return True
            Catch ex As Exception
                xmlWS(ex.ToString)
                Return -1
                'Response.Redirect("/Globales/error.aspx", True)
            End Try
        End Using
    End Function

    Public Shared Sub xmlWS(ByVal sMsg As String)
        Dim fechaHoy As DateTime = DateTime.Today
        Dim fDia As String = fechaHoy.Day
        Dim fMes As String = fechaHoy.Month
        Dim fAnho As String = fechaHoy.Year
        Dim fHora As String = Convert.ToString(fechaHoy.TimeOfDay)
        Dim sFecha = fDia + "-" + fMes + "-" + fAnho + "-"

        'Dim Ruta As String = IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.GetName.CodeBase).Remove(0, 6) '"E:\PRUEBAS\SI10VentasProd\Bin\logs" '
        Dim Ruta As String = "W:\BClogs\"
        Try
            Dim oSW As New StreamWriter(Ruta & "\" + sFecha + "BC.txt", True)
            Dim scomando As String = String.Empty
            oSW.WriteLine("-! " & Now & " || Log: " & sMsg)
            oSW.Flush()
            oSW.Close()
        Catch ex As Exception

        End Try

    End Sub

    Public Shared Function GetValorCodRazon(ByVal codigo As String) As String
        Select Case codigo
            Case "01"
                Return "Nivel de Endeudamiento"
            Case "04"
                Return "Consulta reciente"
            Case "05"
                Return "Pago vencido reciente"
            Case "07"
                Return "Cuentas abiertas con morosidad"
            Case "09"
                Return "Bajo promedio de antigüedad en créditos abiertos"
            Case "12"
                Return "Tipo de crédito con mayor riesgo"
            Case "13"
                Return "Número de cuentas abiertas"
            Case "14"
                Return "Relación entre créditos revolventes y no revolventes"
            Case "15"
                Return "Utilización significativa de límites de crédito revolventes"
            Case "16"
                Return "Tiempo desde última cuenta aperturada"
            Case "17"
                Return "Meses desde último atraso"
            Case "18"
                Return "Duración de cuenta abierta más antigua"
            Case "20"
                Return "Relación entre cuentas con morosidad y sin morosidad"
            Case "21"
                Return "Atrasos frecuentes o recientes"
            Case "24"
                Return "Créditos con morosidad importante"
            Case "27"
                Return "Varios créditos cerrados"
            Case "28"
                Return "Proporción alta de saldos contra crédito máximo"
            Case "29"
                Return "Proporción de cuentas nuevas en los últimos 24 meses"
            Case "31"
                Return "Atrasos frecuentes o recientes"
            Case "32"
                Return "Relación entre experiencias con y sin morosidad"
            Case "33"
                Return "Tipo de crédito con mayor riesgo"
            Case "34"
                Return "Cuentas con morosidad reciente"
            Case "51"
                Return "Pago adecuado del crédito"
            Case "52"
                Return "Pago adecuado del crédito"
            Case "53"
                Return "Pagos adecuados de los créditos"
            Case "54"
                Return "Pagos adecuados de los créditos"
            Case "55"
                Return "Créditos con morosidad"
            Case "56"
                Return "Créditos nuevos con morosidad"
            Case "57"
                Return "Créditos con historial de morosidad"
            Case "58"
                Return "Créditos con atrasos"
            Case "59"
                Return "Créditos con atrasos mayores a 90 días"
            Case "60"
                Return "Créditos con atrasos mayores a 90 días"
            Case "61"
                Return "Créditos con atrasos mayores a 90 días"
            Case Else
                Return "No se encontró código de razón"
        End Select
    End Function

    Public Function checkExistsContact() As Integer
        Dim connectionString As String = "Data Source=23.251.146.119;Initial Catalog=1767_SistemaSICAS;Persist Security Info=True;User ID=sa;Password=Inteldba01"
        Using connection As New SqlConnection(connectionString)
            Dim sql As String = "SELECT IDCont FROM CatContactos WITH (NOLOCK) WHERE NombreCompleto = @NombreCompleto AND RFC = @RFC"
            Dim command As New SqlCommand(sql, connection)
            command.Parameters.Add("@NombreCompleto", SqlDbType.VarChar)
            command.Parameters("@NombreCompleto").Value = ApellidoPaterno + " " + ApellidoMaterno + " " + PrimerNombre + " " + SegundoNombre
            command.Parameters.Add("@RFC", SqlDbType.VarChar)
            command.Parameters("@RFC").Value = RFC
            Try
                connection.Open()
                Dim sdr As SqlDataReader = command.ExecuteReader()
                sdr.Read()
                IDCont = sdr("IDCont")
                connection.Close()
                Return IDCont
            Catch ex As Exception
                Return 0
            End Try
        End Using
    End Function
    Public Function generateContact() As Boolean
        Dim var As New Random
        Dim cveBit As String
        Dim Fnac = DateAndTime.DateValue("19" & Mid(RFC, 5, 2) & "/" & Mid(RFC, 7, 2) & "/" & Mid(RFC, 9, 2))
        cveBit = var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999)
        'Query de guardado de información
        Dim connectionString As String = "Data Source=23.251.146.119;Initial Catalog=1767_SistemaSICAS;Persist Security Info=True;User ID=sa;Password=Inteldba01"
        Using connection As New SqlConnection(connectionString)
            'connection.Open()
            Dim sql As String
            If email <> Nothing Then
                sql = "INSERT INTO CatContactos (ApellidoP, ApellidoM, Nombre, FechaNac, RFC, email1, NombreCompleto, FechaCap, ClaveBit) OUTPUT Inserted.IDCont " &
                                "values (@ApellidoP, @ApellidoM, @Nombres, @FechaNac, @RFC, @email, @NombreCompleto, @FechaAlta, @ClaveBit)"
            Else
                sql = "INSERT INTO CatContactos (ApellidoP, ApellidoM, Nombre, FechaNac, RFC, NombreCompleto, FechaCap, ClaveBit) OUTPUT Inserted.IDCont " &
                                "values (@ApellidoP, @ApellidoM, @Nombres, @FechaNac, @RFC, @NombreCompleto, @FechaAlta, @ClaveBit)"
            End If

            Dim command As New SqlCommand(sql, connection)
            command.Parameters.Add("@ApellidoP", SqlDbType.VarChar)
            command.Parameters("@ApellidoP").Value = ApellidoPaterno
            command.Parameters.Add("@ApellidoM", SqlDbType.VarChar)
            command.Parameters("@ApellidoM").Value = ApellidoMaterno
            command.Parameters.Add("@Nombres", SqlDbType.VarChar)
            command.Parameters("@Nombres").Value = PrimerNombre & " " & SegundoNombre
            command.Parameters.Add("@FechaNac", SqlDbType.SmallDateTime)
            command.Parameters("@FechaNac").Value = Fnac
            command.Parameters.Add("@RFC", SqlDbType.NVarChar)
            command.Parameters("@RFC").Value = RFC
            command.Parameters.Add("@NombreCompleto", SqlDbType.VarChar)
            command.Parameters("@NombreCompleto").Value = ApellidoPaterno & " " & ApellidoMaterno & " " & PrimerNombre & " " & SegundoNombre
            command.Parameters.Add("@FechaAlta", SqlDbType.SmallDateTime)
            command.Parameters("@FechaAlta").Value = Date.Now
            command.Parameters.Add("@ClaveBit", SqlDbType.VarChar)
            command.Parameters("@ClaveBit").Value = cveBit
            If email <> Nothing Then
                command.Parameters.Add("@email", SqlDbType.VarChar)
                command.Parameters("@email").Value = email
            End If
            Try
                connection.Open()
                Dim sdr As SqlDataReader = command.ExecuteReader()
                sdr.Read()
                IDCont = sdr("IDCont")
                connection.Close()
                Return True
            Catch ex As Exception
                xmlWS(ex.ToString)
                Return False
            End Try
        End Using
    End Function
    Sub generateProspect(ByVal IDCont As Integer, ByVal Score As String)
        Dim var As New Random
        Dim cveBit As String
        cveBit = var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999)
        Dim IDDir As Integer = generateAddress(IDCont)

        Dim connectionString As String = "Data Source=23.251.146.119;Initial Catalog=1767_SistemaSICAS;Persist Security Info=True;User Id=sa;Password=Inteldba01"
        Dim sql As String = "INSERT INTO DatProspectos (IDCont, IDDir, IDContacto, FCaptura, Obs, ClaveBit, IDCamp, IDUser) OUTPUT Inserted.IDPros " &
                            "Values (@IDCont,@IDDir,@IDCont,@FCaptura, @Observaciones, @CveBit, @IDCamp,  @IDUser) "
        Score = Replace(Score, "</br>", "")
        Using connection As New SqlConnection(connectionString)
            Dim command As New SqlCommand(sql, connection)
            command.Parameters.Add("@IDCont", SqlDbType.Int)
            command.Parameters("@IDCont").Value = Convert.ToInt32(IDCont)
            command.Parameters.Add("@IDDir", SqlDbType.Int)
            command.Parameters("@IDDir").Value = Convert.ToInt32(IDDir)
            command.Parameters.Add("@FCaptura", SqlDbType.SmallDateTime)
            command.Parameters("@FCaptura").Value = Date.Now
            command.Parameters.Add("@Observaciones", SqlDbType.NText)
            command.Parameters("@Observaciones").Value = Score
            command.Parameters.Add("@CveBit", SqlDbType.NVarChar)
            command.Parameters("@CveBit").Value = cveBit
            command.Parameters.Add("@IDCamp", SqlDbType.NVarChar)
            command.Parameters("@IDCamp").Value = "-1"
            command.Parameters.Add("@IDUser", SqlDbType.NVarChar)
            command.Parameters("@IDUser").Value = "1"
            Try
                connection.Open()
                Dim sdr As SqlDataReader = command.ExecuteReader()
                sdr.Read()
                IDPros = sdr("IDPros")
                connection.Close()
            Catch ex As Exception
                xmlWS(ex.ToString)
                Session("error") = "Error al Dar de Alta al Prospecto"
                'Response.Redirect("/Globales/error.aspx", True)
            End Try
        End Using

    End Sub
    Public Function generateAddress(ByVal IDDir As Integer) As Integer
        Dim var As New Random
        Dim cveBit As String
        cveBit = var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999) & var.Next(100, 999)
        Dim connectionString As String = "Data Source=23.251.146.119;Initial Catalog=1767_SistemaSICAS;Persist Security Info=True;User Id=sa;Password=Inteldba01"
        Dim sql As String = "INSERT INTO CatDirecciones (Calle, NOExt, NOInt, CPostal, Colonia, Poblacion, Ciudad, ClaveBit) OUTPUT Inserted.IDDir " &
                            "Values (@Calle,@NoExt,@NoInt,@Cpostal, @Colonia, @Poblacion, @Ciudad, @CveBit) "
        Using connection As New SqlConnection(connectionString)
            Dim command As New SqlCommand(sql, connection)
            command.Parameters.Add("@Calle", SqlDbType.NVarChar)
            command.Parameters("@Calle").Value = Calle
            command.Parameters.Add("@NoExt", SqlDbType.NVarChar)
            command.Parameters("@NoExt").Value = NoExt
            command.Parameters.Add("@NoInt", SqlDbType.NVarChar)
            command.Parameters("@NoInt").Value = NoInt
            command.Parameters.Add("@CPostal", SqlDbType.NVarChar)
            command.Parameters("@CPostal").Value = CPostal
            command.Parameters.Add("@Colonia", SqlDbType.NVarChar)
            command.Parameters("@Colonia").Value = Colonia
            command.Parameters.Add("@Poblacion", SqlDbType.NVarChar)
            command.Parameters("@Poblacion").Value = Estado
            command.Parameters.Add("@Ciudad", SqlDbType.NVarChar)
            command.Parameters("@Ciudad").Value = DelegMunic
            command.Parameters.Add("@CveBit", SqlDbType.NVarChar)
            command.Parameters("@CveBit").Value = cveBit
            Try
                connection.Open()
                Dim sdr As SqlDataReader = command.ExecuteReader()
                sdr.Read()
                IDDir = sdr("IDDir")
                connection.Close()
                Return IDDir
            Catch ex As Exception
                xmlWS(ex.ToString)
                Session("error") = "Error al Dar de Alta la dirección"
                Return -1
                'Response.Redirect("/Globales/error.aspx", True)
            End Try
        End Using
    End Function

    Public Function saveScore(ByVal Score As String, ByVal NumeroControl As String) As Boolean
        Dim connectionString As String = "Data Source=23.251.146.119;Initial Catalog=BuroCredito;Persist Security Info=True;User Id=sa;Password=Inteldba01"
        Dim sql As String = "INSERT INTO ControlScore (Score,NumeroControlConsulta) OUTPUT Inserted.Score " &
                            "Values (@Score, @NumControl) "
        If Score = Nothing Then
            Score = ""
        End If
        If Score.Contains("NO AUTENTICADO") Then
            Score = ""
        End If
        Using connection As New SqlConnection(connectionString)
            Dim command As New SqlCommand(sql, connection)
            command.Parameters.Add("@Score", SqlDbType.NVarChar)
            command.Parameters("@Score").Value = Score
            command.Parameters.Add("@NumControl", SqlDbType.NVarChar)
            command.Parameters("@NumControl").Value = NumeroControl
            Try
                connection.Open()
                Dim sdr As SqlDataReader = command.ExecuteReader()
                sdr.Read()
                NoSol = sdr("Score")
                connection.Close()
                Return True
            Catch ex As Exception
                xmlWS(ex.ToString)
                Session("error") = "Error al guardar Score"
                Return -1
                'Response.Redirect("/Globales/error.aspx", True)
            End Try
        End Using
    End Function

End Class